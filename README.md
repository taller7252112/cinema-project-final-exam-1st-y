# Parcial Final - Sistema de Gestión de Ventas de Boletería

Este proyecto implementa un sistema de gestión de ventas de boletos para una sala de eventos. Proporciona funciones para mostrar el plano de la sala, realizar ventas de boletos, cambiar sillas, y mostrar ingresos por ventas, entre otras.

## Funcionalidades

1. **Mostrar Plano de la Sala:**
   - Muestra una representación visual de los asientos disponibles en la sala.

2. **Vender un Boleto:**
   - Calcula el valor del boleto según la fila seleccionada.
   - Registra la venta y la categoría correspondiente (Categoría 1, 2 o 3).

3. **Cambiar Silla:**
   - Permite cambiar una silla existente por otra dentro de la sala.
   - Verifica la validez de las posiciones actuales y nuevas.

4. **Mostrar Ingresos por Venta:**
   - Calcula y muestra los ingresos totales por ventas.

5. **Consultar Sillas Ocupadas de Toda la Sala:**
   - Muestra la cantidad de sillas ocupadas por categoría y en total.

## Instrucciones de Uso

1. Ejecute el programa y siga las instrucciones del menú para realizar las operaciones deseadas.
2. Al vender un boleto, ingrese la posición de la silla en el formato `Ai-j`.
3. Para cambiar una silla, especifique la posición actual y la nueva posición.

## Precios de los Boletos

- Fila 1 a 2: $10,000 COP.
- Fila 3 a 5: $20,000 COP.
- Fila 6 a 8: $30,000 COP.

## Estructura del Repositorio

- **master:** Contiene la versión principal del código.
- **feature:** Incluye funcionalidades adicionales o en desarrollo.


